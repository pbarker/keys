This repository contains SSH, PGP, etc keys for
[Paul Barker \<paul@pbarker.dev\>](mailto:paul@pbarker.dev).

The canonical url for this repository is https://git.sr.ht/~pbarker/keys.

The content of this repository is just data so it's not expected to be
copyrightable. However, to avoid any doubt, if copyright does apply to any of
the data here then it is released under the CC0 license ([see LICENSE](LICENSE)).
